@extends('layouts.client')

@section('styles')
@endsection

@section('content')
    <main role="main" class="main-content">
        <div>
            <section class="contact spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 text-center">
                            <div class="contact__widget">
                                <span class="icon_phone"></span>
                                <h4>Phone</h4>
                                <p>+26377 281 7517</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 text-center">
                            <div class="contact__widget">
                                <span class="icon_pin_alt"></span>
                                <h4>Address</h4>
                                <p>15 Trail Rd Mt Pleasant</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 text-center">
                            <div class="contact__widget">
                                <span class="icon_clock_alt"></span>
                                <h4>Open time</h4>
                                <p>08:00 am to 17:00 pm</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 text-center">
                            <div class="contact__widget">
                                <span class="icon_mail_alt"></span>
                                <h4>Email</h4>
                                <p>info@unassailable.co.zw</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4619.793331196595!2d31.04496641239326!3d-17.754878313715338!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1931af9a3fa475db%3A0x8272c34a912a9881!2sMount%20Pleasant%2C%20Harare!5e0!3m2!1sen!2szw!4v1665548225605!5m2!1sen!2szw" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade" height="500" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                <div class="map-inside">
                    <i class="icon_pin"></i>
                    <div class="inside-widget">
                        <h4>Harare</h4>
                        <ul>
                            <li>Phone: +263 719 999 9999</li>
                            <li>Add: 11 Trail Rd. Mt Pleasant, Hre</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="contact-form spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="contact__form__title">
                                <h2>Leave Message</h2>
                            </div>
                        </div>
                    </div>
                    <form action="{{route('send-mail')}}">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <input type="text" name="name" placeholder="Your name">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <input type="text" name="email" placeholder="Your Email">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <input type="text" name="subject" placeholder="Subject">
                            </div>
                            <div class="col-lg-12 text-center">
                                <textarea placeholder="Your message"></textarea>
                                <button type="submit" class="site-btn">SEND MESSAGE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
@endsection
