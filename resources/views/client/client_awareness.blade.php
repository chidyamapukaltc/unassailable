@extends('layouts.client')

@section('styles')
@endsection

@section('content')
    <main role="main" class="main-content">
        <div>
            <section class="blog spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/gbv.jpeg')}}" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="{{route('client.gbv')}}" style="color: #FFFFFF">Gender-based violence (GBV)</a></h5>
                                            <p>GBV is violence directed against a person because of that person's gender or... </p>
                                            <a href="{{route('client.gbv')}}" class="blog__btn" style="color: #FFFFFF">READ MORE <span
                                                    class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/depression.jpeg')}}" height="224" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="{{route('client.depression')}}"  style="color: #FFFFFF">Depression</a></h5>
                                            <p>Depression is a mood disorder that causes feelings of sadness that won't go away....
                                            </p>
                                            <a href="{{route('client.gbv')}}" class="blog__btn" style="color: #FFFFFF">READ MORE <span
                                                    class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/anxiety2.jpeg')}}" height="224" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="{{route('client.anxiety')}}"  style="color: #FFFFFF">Anxiety</a></h5>
                                            <p>Anxiety is a feeling of fear, dread, and uneasiness. It might cause you to sweat... </p>
                                            <a href="{{route('client.anxiety')}}" class="blog__btn" style="color: #FFFFFF">READ MORE <span
                                                    class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection

@section('scripts')
@endsection
