@extends('layouts.client')

@section('styles')
@endsection

@section('content')
    <main role="main" class="main-content">
        <div>
            <section class="blog-details spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 order-md-1 order-1">
                            <div class="blog__details__text">
                                <div class="section-title related-blog-title">
                                    <h2 >Anxiety</h2>
                                </div>
                                <img src="{{url('assets/images/anxiety2.jpeg')}}"  alt="">
                                <p>Anxiety is a feeling of fear, dread, and uneasiness. It might cause you to sweat, feel restless and tense, and have a rapid heartbeat. It can be a normal reaction to stress. For example, you might feel anxious when faced with a difficult problem at work, before taking a test, or before making an important decision. It can help you to cope. The anxiety may give you a boost of energy or help you focus. But for people with anxiety disorders, the fear is not temporary and can be overwhelming</p>

                                <h5 >What are anxiety disorders?</h5>



                                <p>Anxiety disorders are conditions in which you have anxiety that does not go away and can get worse over time. The symptoms can interfere with daily activities such as job performance, schoolwork, and relationships.</p>

                                <h5> Types of anxiety disorders</h5>
                                <ul class="article-list">

                                    <li> Generalized anxiety disorder (GAD).People with GAD worry about ordinary issues such as health, money, work, and family. But their
                                        worries are excessive, and they have them almost every day for at least 6 months.</li>
                                    <li> Panic disorder. People with panic disorder have panic attacks. These are sudden, repeated periods of intense fear
                                        when there is no danger. The attacks come on quickly and can last several minutes or more.</li>
                                    <li>Phobias. People with phobias have an intense fear of something that poses little or no actual danger.
                                        Their fear may be about spiders, flying, going to crowded places, or being in social situations (known as social anxiety).</li>
                                </ul>
                               <p> The cause of anxiety is unknown. Factors such as genetics, brain biology and chemistry, stress, and your environment may play a role.</p>

                                <h5> Who is at risk for anxiety disorders?</h5>
                                <p>The risk factors for the different types of anxiety disorders can vary. For example, GAD and phobias are more common in women,
                                    but social anxiety affects men and women equally. There are some general risk factors for all types of anxiety disorders, including
                                Certain personality traits, such as being shy or withdrawn when you are in new situations or meeting new people. Traumatic events in early childhood or adulthood. Family history of anxiety or other
                                    mental disorders</p>


                                <p>The different types of anxiety disorders can have different symptoms. But they all have a combination of:</p>
                                <ul class="article-list">
                                    <li> Anxious thoughts or beliefs that are hard to control. They make you feel restless and tense and interfere with your daily life. They do not go away and can get worse over time.</li>
                                    <li> Physical symptoms, such as a pounding or rapid heartbeat, unexplained aches and pains, dizziness, and shortness of breath</li>
                                    <li> Changes in behavior, such as avoiding everyday activities you used to do</li>

                                </ul>
                                <p>  Using caffeine, other substances, and certain medicines can make your symptoms worse. To diagnose anxiety disorders, your health care provider will ask about your symptoms and medical history. You may also have a physical exam and lab tests to make sure that a different health problem is not the cause of your symptoms.
                                    If you don't have another health problem, you will get a psychological evaluation. Your provider may do it, or you may be referred to a mental health professional to get one.</p>
                                <p>The main treatments for anxiety disorders are psychotherapy (talk therapy), medicines, or both:</p>
                                <ul class="article-list">
                                <li> Cognitive behavioral therapy (CBT) is a type of psychotherapy that is often used to treat anxiety disorders.
                                    CBT teaches you different ways of thinking and behaving. It can help you change how you react to the things that cause you to feel fear and anxiety.
                                    It may include exposure therapy. This focuses on having you confront your fears so that you will be able to do the things that you had been avoiding.</li>
                                <li> Medicines to treat anxiety disorders include anti-anxiety medicines and certain antidepressants. Some types of medicines may work better for specific types of anxiety disorders.
                                    You should work closely with your health care provider to identify which medicine is best for you. You may need to try more than one medicine before you can find the right one.</li>
                                </ul>



                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="related-blog spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title related-blog-title">
                                <h2 >Post You May Like</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/gbv.jpeg')}}" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="{{route('client.gbv')}}">Gender-based violence (GBV)</a></h5>
                                            <p>GBV is violence directed against a person because of that person's gender
                                                or...</p>
                                            <a href="{{route('client.gbv')}}" class="blog__btn" >READ MORE <span
                                                    class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/groupsunday.jpeg')}}" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 29,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="https://www.sundaymail.co.zw/new-group-offers-free-counselling-for-gbv-victims"  >Sunday Mail-Gender-based violence</a></h5>
                                            <p>AS Zimbabwe commemorates 16 Days of Activism against Gender-Based Violence (GBV), a local women’s rights organisation, Unassailable, is helping victims get free counselling. </p>
                                            <a href="https://www.sundaymail.co.zw/new-group-offers-free-counselling-for-gbv-victims" class="blog__btn white-text"  >READ MORE <span class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/depression.jpeg')}}" height="224" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="{{route('client.depression')}}"  >Depression</a></h5>
                                            <p>Depression is a mood disorder that causes feelings of sadness that won't go away....
                                            </p>
                                            <a href="{{route('client.gbv')}}" class="blog__btn" >READ MORE <span
                                                    class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection

@section('scripts')
@endsection
