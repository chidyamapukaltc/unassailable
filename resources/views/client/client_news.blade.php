@extends('layouts.client')

@section('styles')
@endsection

@section('content')
    <main role="main" class="main-content">
        <div>
            <section class="hero"  >
                <div class="container" >
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="hero__search">


                            </div>
                            <iframe width="1140" height="350" src="{{url('assets/images/vid.mp4')}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


                        </div>
                    </div>
                </div>
            </section>
            <section class="blog spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/groupsunday.jpeg')}}" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 29,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="https://www.sundaymail.co.zw/new-group-offers-free-counselling-for-gbv-victims"  >Sunday Mail-Gender-based violence</a></h5>
                                            <p>AS Zimbabwe commemorates 16 Days of Activism against Gender-Based Violence (GBV), a local women’s rights organisation, Unassailable, is helping victims get free counselling. </p>
                                            <a href="https://www.sundaymail.co.zw/new-group-offers-free-counselling-for-gbv-victims" class="blog__btn white-text"  style="color: #FFFFFF">READ MORE <span class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/deoneunassailable.jpeg')}}" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 29,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="https://startupbiz.co.zw/unassailable-the-personal-security-business-that-wants-to-protect-women/" >UNASSAILABLE; THE PERSONAL SECURITY BUSINESS THAT WANTS TO PROTECT WOMEN</a></h5>
                                            <p>Unassailable is a young business that was established in May 2021. “I started Unassailable after realising that there was a rise in the number of gender-based violence.. </p>
                                            <a href="https://startupbiz.co.zw/unassailable-the-personal-security-business-that-wants-to-protect-women/" class="blog__btn" >READ MORE <span class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/deoneunassailable.jpeg')}}" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="https://www.sundaymail.co.zw/new-initiative-to-protect-women-against-abuse" >New initiative to protect women against abuse </a></h5>
                                            <p>GBV is violence directed against a person because of that person's gender or violence that affects persons of a particular gender disproportionately... </p>
                                            <a href="https://www.sundaymail.co.zw/new-initiative-to-protect-women-against-abuse" class="blog__btn" >READ MORE <span class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection

@section('scripts')
@endsection
