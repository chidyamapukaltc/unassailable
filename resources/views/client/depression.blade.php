@extends('layouts.client')

@section('styles')
@endsection

@section('content')
    <main role="main" class="main-content">
        <div>
            <section class="blog-details spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 order-md-1 order-1">
                            <div class="blog__details__text">
                                <div class="section-title related-blog-title">
                                    <h2>Depression</h2>
                                </div>
                                <img src="{{url('assets/images/depression1.jpg')}}" alt="">
                                <p>Depression is a mood disorder that causes feelings of sadness that won't go away.
                                    Unfortunately, there's a lot of stigma around depression. Depression isn't a
                                    weakness or a character flaw. It's not about being in a bad mood, and people who
                                    experience depression can't just snap out of it. Depression is a common, serious,
                                    and treatable condition. If you're experiencing depression, you're not alone. It
                                    honestly affects people of all ages and races and biological sexes, income levels
                                    and educational backgrounds. Approximately one in six people will experience a major
                                    depressive episode at some point in their lifetime, while up to 16 million adults
                                    each year suffer from clinical depression. There are many types of symptoms that
                                    make up depression. Emotionally, you may feel sad or down or irritable or even
                                    apathetic. Physically, the body really slows down. You feel tired. Your sleep is
                                    often disrupted. It's really hard to get yourself motivated. Your thinking also
                                    changes. It can just be hard to concentrate. Your thoughts tend to be much more
                                    negative. You can be really hard on yourself, feel hopeless and helpless about
                                    things. And even in some cases, have thoughts of not wanting to live. Behaviorally,
                                    you just want to pull back and withdraw from others, activities, and day-to-day
                                    responsibilities. These symptoms all work together to keep you trapped in a cycle of
                                    depression. Symptoms of depression are different for everyone. Some symptoms may be
                                    a sign of another disorder or medical condition.
                                    That's why it's important to get an accurate diagnosis.</p>

                                <h5 >What causes depression?</h5>
                                <p> While there's no single cause of depression, most experts believe there's a
                                    combination of biological, social, and psychological factors that contribute to
                                    depression risk. Biologically, we think about genetics or a family history of
                                    depression, health conditions such as diabetes, heart disease or thyroid disorders,
                                    and even hormonal changes that happen over the lifespan, such as pregnancy and
                                    menopause. Changes in brain chemistry, especially disruptions in neurotransmitters
                                    like serotonin, that play an important role in regulating many bodily functions,
                                    including mood, sleep, and appetite, are thought to play a particularly important
                                    role in depression. Socially stressful and traumatic life events, limited access to
                                    resources such as food, housing, and health care, and a lack of social support all
                                    contribute to depression risk. Psychologically, we think of how negative thoughts
                                    and problematic coping behaviors, such as avoidance and substance use, increase our
                                    vulnerability to depression.
                                    The good news is that treatment helps. Effective treatments for depression exist and
                                    you do have options to see what works best for you. Lifestyle changes that improve
                                    sleep habits, exercise, and address underlying health conditions can be an important
                                    first step. Medications such as antidepressants can be helpful in alleviating
                                    depressive symptoms. Therapy, especially cognitive behavioral therapy, teaches
                                    skills to better manage negative thoughts and improve coping behaviors to help break
                                    you out of cycles of depression. Whatever the cause, remember that depression is not
                                    your fault, and it can be treated.
                                    To help diagnose depression, your health care provider may use a physical exam, lab
                                    tests, or a mental health evaluation. These results will help identify various
                                    treatment options that best fit your situation.
                                    Help is available. You don't have to deal with depression by yourself. Take the next
                                    step and reach out. If you're hesitant to talk to a health care provider, talk to a
                                    friend or loved one about how to get help. Living with depression isn't easy and
                                    you're not alone in your struggles. Always remember that effective treatments and
                                    supports are available to help you start feeling better. Want to learn more about
                                    depression? Visit mayoclinic.org. Do take care.
                                    Depression is a mood disorder that causes a persistent feeling of sadness and loss
                                    of interest. Also called major depressive disorder or clinical depression, it
                                    affects how you feel, think and behave and can lead to a variety of emotional and
                                    physical problems. You may have trouble doing normal day-to-day activities, and
                                    sometimes you may feel as if life isn't worth living. More than just a bout of the
                                    blues, depression isn't a weakness and you can't simply "snap out" of it. Depression
                                    may require long-term treatment. But don't get discouraged. Most people with
                                    depression feel better with medication, psychotherapy or both.</p>


                                <h5 >Symptoms</h5>
                                <p>Although depression may occur only once during your life, people typically have
                                    multiple episodes. During these episodes, symptoms occur most of the day, nearly
                                    every day and may include:</p>
                                <ul class="article-list">
                                    <li>
                                        Feelings of sadness, tearfulness, emptiness or hopelessness
                                    </li>
                                    <li>
                                        Angry outbursts, irritability or frustration, even over small matters
                                    </li>
                                    <li>
                                        Loss of interest or pleasure in most or all normal activities, such as sex,
                                        hobbies or sports
                                    </li>

                                    <li>
                                        Sleep disturbances, including insomnia or sleeping too much
                                    </li>
                                    <li>
                                        Tiredness and lack of energy, so even small tasks take extra effort
                                    </li>
                                    <li>
                                        Reduced appetite and weight loss or increased cravings for food and weight gain
                                    </li>
                                    <li>
                                        Anxiety, agitation or restlessness
                                    </li>
                                    <li>
                                        Slowed thinking, speaking or body movements
                                    </li>
                                    <li>
                                        Feelings of worthlessness or guilt, fixating on past failures or self-blame
                                    </li>

                                    <li>
                                        Trouble thinking, concentrating, making decisions and remembering things
                                    </li>
                                    <li>
                                        Frequent or recurrent thoughts of death, suicidal thoughts, suicide attempts or
                                        suicide
                                    </li>
                                    <li>
                                        Unexplained physical problems, such as back pain or headaches
                                    </li>

                                </ul>

                                <p>For many people with depression, symptoms usually are severe enough to cause
                                    noticeable problems in day-to-day activities, such as work, school,
                                    social activities or relationships with others. Some people may feel generally
                                    miserable or unhappy without really knowing why.</p>


                                <h5 >Depression symptoms in older adults</h5>

                                <p> Depression is not a normal part of growing older, and it should never be taken
                                    lightly. Unfortunately, depression often goes undiagnosed and untreated in older
                                    adults,
                                    and they may feel reluctant to seek help. Symptoms of depression may be different or
                                    less obvious in older adults, such as:</p>
                                <ul class="article-list">
                                    <li>Memory difficulties or personality changes</li>
                                    <li> Physical aches or pain</li>
                                    <li>Fatigue, loss of appetite, sleep problems or loss of interest in sex — not
                                        caused by a medical condition or medication
                                    </li>
                                    <li> Often wanting to stay at home, rather than going out to socialize or doing new
                                        things
                                    </li>
                                    <li>Suicidal thinking or feelings, especially in older men</li>
                                </ul>
                                <h5 >Causes</h5>
                                <p>It's not known exactly what causes depression. As with many mental disorders, a
                                    variety of factors may be involved, such as:</p>
                                <ul class="article-list">

                                    <li>Biological differences. People with depression appear to have physical changes
                                        in their brains. The significance of these changes is still uncertain, but may
                                        eventually help pinpoint causes.
                                    </li>
                                    <li> Brain chemistry. Neurotransmitters are naturally occurring brain chemicals that
                                        likely play a role in depression. Recent research indicates that changes in the
                                        function and effect of these
                                        neurotransmitters and how they interact with neurocircuits involved in
                                        maintaining mood stability may play a significant role in depression and its
                                        treatment.
                                    </li>
                                    <li> Hormones. Changes in the body's balance of hormones may be involved in causing
                                        or triggering depression. Hormone changes can result with pregnancy and during
                                        the weeks or months
                                        after delivery (postpartum) and from thyroid problems, menopause or a number of
                                        other conditions.
                                    </li>
                                    <li>Inherited traits. Depression is more common in people whose blood relatives also
                                        have this condition. Researchers are trying to find genes that may be involved
                                        in causing depression.
                                    </li>
                                </ul>

                                <p> When to see a doctor If you feel depressed, make an appointment to see your doctor
                                    or mental health professional as soon as you can. If you're reluctant to seek
                                    treatment, talk to a friend or loved one, any health care professional, a faith
                                    leader, or someone else you trust.</p>

                                <h5 >Risk factors</h5>

                                <p> Depression often begins in the teens, 20s or 30s, but it can happen at any age. More
                                    women than men are diagnosed with depression, but this may be due in part because
                                    women are more likely to seek treatment.
                                    Factors that seem to increase the risk of developing or triggering depression
                                    include:</p>
                                <ul class="article-list">
                                    <li> Certain personality traits, such as low self-esteem and being too dependent,
                                        self-critical or pessimistic
                                    </li>
                                    <li>Traumatic or stressful events, such as physical or sexual abuse, the death or
                                        loss of a loved one, a difficult relationship, or financial problems
                                    </li>
                                    <li>Blood relatives with a history of depression, bipolar disorder, alcoholism or
                                        suicide
                                    </li>
                                    <li> Being lesbian, gay, bisexual or transgender, or having variations in the
                                        development of genital organs that aren't clearly male or female (intersex) in
                                        an unsupported situation
                                    </li>
                                    <li>History of other mental health disorders, such as anxiety disorder, eating
                                        disorders or post-traumatic stress disorder
                                    </li>
                                    <li>Abuse of alcohol or recreational drugs</li>
                                    <li>Serious or chronic illness, including cancer, stroke, chronic pain or heart
                                        disease
                                    </li>
                                    <li> Certain medications, such as some high blood pressure medications or sleeping
                                        pills (talk to your doctor before stopping any medication)
                                    </li>
                                </ul>
                                <h5 >Prevention</h5>

                                <p> There's no sure way to prevent depression. However, these strategies may help.</p>
                                <ul class="article-list">
                                    <li> Take steps to control stress, to increase your resilience and boost your
                                        self-esteem.
                                    </li>
                                    <li>Reach out to family and friends, especially in times of crisis, to help you
                                        weather rough spells.
                                    </li>
                                    <li> Get treatment at the earliest sign of a problem to help prevent depression from
                                        worsening.
                                    </li>
                                    <li> Consider getting long-term maintenance treatment to help prevent a relapse of
                                        symptoms.
                                    </li>
                                </ul>


                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="related-blog spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title related-blog-title">
                                <h2>Post You May Like</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/gbv.jpeg')}}" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="{{route('client.gbv')}}" >Gender-based violence (GBV)</a></h5>
                                            <p>GBV is violence directed against a person because of that person's gender or... </p>
                                            <a href="{{route('client.gbv')}}" class="blog__btn" >READ MORE <span
                                                    class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/anxiety2.jpeg')}}" height="224" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="{{route('client.anxiety')}}" >Anxiety</a></h5>
                                            <p>Anxiety is a feeling of fear, dread, and uneasiness. It might cause you to sweat... </p>
                                            <a href="{{route('client.anxiety')}}" class="blog__btn" >READ MORE <span
                                                    class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/groupsunday.jpeg')}}" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 29,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="https://www.sundaymail.co.zw/new-group-offers-free-counselling-for-gbv-victims" >Sunday Mail-Gender-based violence</a></h5>
                                            <p>AS Zimbabwe commemorates 16 Days of Activism against Gender-Based Violence (GBV), a local women’s rights organisation, Unassailable, is helping victims get free counselling. </p>
                                            <a href="https://www.sundaymail.co.zw/new-group-offers-free-counselling-for-gbv-victims" class="blog__btn white-text"  >READ MORE <span class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection

@section('scripts')
@endsection
