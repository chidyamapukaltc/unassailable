@extends('layouts.client')

@section('styles')
@endsection

@section('content')
    <main role="main" class="main-content">
        <div>
            @livewire('client-product-details', ['productId'=>$productId])
        </div>
    </main>
@endsection

@section('scripts')
@endsection
