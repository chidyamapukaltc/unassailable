@extends('layouts.client')

@section('styles')
@endsection

@section('content')
    <main role="main" class="main-content">
        <div>
            <section class="blog-details spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 order-md-1 order-1">
                            <div class="blog__details__text">
                                <div class="section-title related-blog-title">
                                    <h2 >Gender Based Violence</h2>
                                </div>
                                <img src="{{url('client/img/gbv3.jpg')}}"  alt="">
                                <p>GBV is violence directed against a person because of that person's gender or violence that affects persons of a particular gender disproportionately.
                                    Violence against women is understood as a violation of human rights and a form of discrimination against women and shall mean all acts of gender-based violence that result in, or are likely to result in physical harm,
                                     sexual harm, psychological or economic harm or suffering to women. It can include violence against women, domestic violence against women, men or children living in the same domestic unit. Although women and girls are the main victims of GBV, it also causes severe harm to families and communities.</p>

                                    <h5>Forms of gender based violence:</h5>
                                <ul class="article-list">
                                    <li>
                                        Physical: it results in injuries, distress and health problems. Typical forms of physical violence are beating, strangling, pushing, and the use of weapons.
                                    </li>
                                    <li>
                                        Sexual: it includes sexual acts, attempts to obtain a sexual act, acts to traffic, or acts otherwise directed against a person’s sexuality without the person’s consent. It’s estimated that one in 20 women (5 %) has been raped in Zimbabwe since the age of 15.
                                    </li>
                                    <li>
                                        Psychological: includes psychologically abusive behaviours, such as controlling, coercion, economic violence and blackmail.
                                    </li>
                                </ul>
                                <h5 >Examples of gender-based violence</h5>
                                <ul class="article-list">
                                    <li>
                                        Domestic violence includes all acts of physical, sexual, psychological and economic violence that occur within the family, domestic unit, or between intimate partners. These can be former or current spouses also when they don’t share the same residence. 22 % of all women who have (had) a partner have experienced physical and/or sexual violence by a partner since the age of 15.
                                    </li>
                                    <li>
                                        Sex-based harassment includes unwelcome verbal, physical or other non-verbal conduct of a sexual nature with the purpose or effect of violating the dignity of a person. Between 45% to 55% of women in Zimbabwe have experienced sexual harassment since the age of 15.
                                    </li>
                                    <li>
                                        Female Genital Mutilation (FGM) is the ritual cutting or removal of some or all of the external female genitalia. It violates women’s bodies and often damages their sexuality, mental health, well-being and participation in their community. It may even lead to death. Today, more than 200 million girls and women alive worldwide have undergone female genital mutilation.
                                    </li>
                                    <li>
                                        Forced marriage refers to marriage concluded under force or coercion – either physical pressure to marry or emotional and psychological pressure. It’s closely linked to child or early marriage, when children are wed before reaching the minimum age for marriage.
                                    </li>
                                    <li>
                                        Online violence is an umbrella term used to describe all sorts of illegal or harmful behaviours against women in the online space. They can be linked to experiences of violence in real life, or be limited to the online environment only. They can include illegal threats, stalking or incitement to violence, unwanted, offensive or sexually explicit emails or messages, sharing of private images or videos without consent, or inappropriate advances on social networking sites.
                                    </li>
                                </ul>
                                <h5 >Solutions</h5>
                                <p>Addressing GBV is a central development goal in its own right, and key to achieve other development outcomes. GBV cannot be separated from issues of human rights economic development, poverty, education, health, peace and justice. Below are some crucial entry points to combat gender-based violence in development cooperation.
                                    Gender based violence is a human rights issue, addressing GBV as a human rights issue empowers survivors of gender-based violence as active rights-holders. International human rights law imposes an absolute prohibition on discrimination in regard to the full enjoyment of all human rights. To meet the peoples human rights obligations we should seek to transform the social and cultural norms regulating power relations between women and men, and other linked aspects of subordination. Recognising GBV as a violation of human rights clarifies the binding obliga- tions internationally to prevent, eradicate and punish such violence.</p>
                                <p>
                                    Tackling gender-based violence is crucial for poverty reduction and economic development As stated in the UN Secretary-General’s in-depth study on all forms of violence against women:
                                    “violence prevents women from contributing to, and benefiting from, development by restricting their choices and limiting their ability to act. The resulting consequences for economic growth and poverty reduction should be of central concern to governments”.
                                    Reducing poverty thus requires a sustained focus on tackling gender-based violence. The physical and psychological harm, fears and threats of gender-based violence, limit women’s and also men’s ability to participate fully in economic, social and political processes that results in decreased productivity and reduced family income. However, poverty and socio-economic insecurity is also one of the factors contributing to GBV, particularly trafficking, early marriage and sexual violence in displacement. Intimate partner violence also tends to increase in contexts of poverty, partly reflecting ideals and expectations linking masculinity to the provider role and subsequent sentiments of ‘failed masculinity’.
                                    The number of years a person spends in school has a positive correlation with a decrease in both future victimisation and perpetration of physical and sexual violence. The empowerment of women through increased income opportunities as a result of education reduces the unequal power relationship between women and men and thus in a long-term perspective has a positive effect on GBV. In the short run however, changed gender relations may increase the prevalence of violence. Schools and other educational institutions do not have a universal nor automatically positive impact on reducing GBV, and can also be sites of GBV, and therefore need to actively promote a gender sensitive, respectful and non-violent culture
                                </p>

                                <p>
                                    Tackling gender based violence is key to protect sexual and reproductive health and Rights and reverse the spread of HIV
                                    Gender-based discrimination and violence, including forced sex, is the cause of many sexual and reproductive health problems including sexually trans-mitted infections and unwanted pregnancy. Unwanted pregnancy can have serious repercussions, including unsafe and illegal abortions, and ostracism by family
                                    members leading to social isolation, suicide and sometimes murder. In addition, violence before, during and after pregnancy is linked to a range of pregnancy-related problems, such as miscarriage, preterm labour or maternal mortality, and efforts to reduce these problems including tackling GBV.
                                    Moreover, tackling GBV is crucial in reversing the spread of HIV.5Tears or abrasions associated with violent sexual encounters increase women’s biological risk of contracting the infection, especially for young girls. Fear of gender-based violence, or the stigma associated with having been victimised, is also an obstacle to accessing information, care and treatment for HIV/AIDS.
                                </p>
                                <p>
                                    Gender based violence is a security concern in the prerequisite for sustainable peace,
                                    although data is scarce, GBV often escalates dramatically during conflict and displacement. As stipulated in UN Security Council Resolution 1325 and subsequent resolutions, women’s rights must be at the centre of conflict prevention and conflict resolution. All parties to armed conflict must make commitments to protect women and girls from GBV, particularly sexual violence, and to combat sexual violence through prohibition, codes of conduct and other means.The need to frame GBV as a security issue and the responsibility to protect goes beyond meeting immediate security needs of women. Implementing globally agreed frameworks is essential for longer-term peace and development.

                                </p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="related-blog spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title related-blog-title">
                                <h2 >Post You May Like</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/depression.jpeg')}}" height="224" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="{{route('client.depression')}}" >Depression</a></h5>
                                            <p>Depression is a mood disorder that causes feelings of sadness that won't go away....
                                            </p>
                                            <a href="{{route('client.gbv')}}" class="blog__btn" >READ MORE <span
                                                    class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/anxiety2.jpeg')}}" height="224" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="{{route('client.anxiety')}}" >Anxiety</a></h5>
                                            <p>Anxiety is a feeling of fear, dread, and uneasiness. It might cause you to sweat... </p>
                                            <a href="{{route('client.anxiety')}}" class="blog__btn">READ MORE <span
                                                    class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/groupsunday.jpeg')}}" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 29,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="https://www.sundaymail.co.zw/new-group-offers-free-counselling-for-gbv-victims"  >Sunday Mail-Gender-based violence</a></h5>
                                            <p>AS Zimbabwe commemorates 16 Days of Activism against Gender-Based Violence (GBV), a local women’s rights organisation, Unassailable, is helping victims get free counselling. </p>
                                            <a href="https://www.sundaymail.co.zw/new-group-offers-free-counselling-for-gbv-victims" class="blog__btn white-text"  >READ MORE <span class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection

@section('scripts')
@endsection
