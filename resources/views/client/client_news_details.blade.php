@extends('layouts.client')

@section('styles')
@endsection

@section('content')
    <main role="main" class="main-content">
        <div>
            <section class="blog-details spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-7 order-md-1 order-1">
                            <div class="blog__details__text">
                                <img src="{{url('assets/images/gbv2.jpeg')}}" alt="">
                                <p>Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet
                                    dui. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Mauris blandit
                                    aliquet elit, eget tincidunt nibh pulvinar a. Vivamus magna justo, lacinia eget consectetur
                                    sed, convallis at tellus. Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada.
                                    Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Proin eget tortor risus.
                                    Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis
                                    quis ac lectus. Donec sollicitudin molestie malesuada. Nulla quis lorem ut libero malesuada
                                    feugiat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>
                                <h3>The corner window forms a place within a place that is a resting point within the large
                                    space.</h3>
                                <p>The study area is located at the back with a view of the vast nature. Together with the other
                                    buildings, a congruent story has been managed in which the whole has a reinforcing effect on
                                    the components. The use of materials seeks connection to the main house, the adjacent
                                    stables</p>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <section class="related-blog spad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title related-blog-title">
                                <h2>Post You May Like</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/gbv.jpeg')}}" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="#">Gender-based violence (GBV)</a></h5>
                                            <p>GBV is violence directed against a person because of that person's gender or...</p>
                                            <a href="/client-news-details" class="blog__btn">READ MORE <span class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/gbv.jpeg')}}" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="#">Gender-based violence (GBV)</a></h5>
                                            <p>GBV is violence directed against a person because of that person's gender or...</p>
                                            <a href="/client-news-details" class="blog__btn">READ MORE <span class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="blog__item">
                                        <div class="blog__item__pic">
                                            <img src="{{url('assets/images/gbv.jpeg')}}" alt="">
                                        </div>
                                        <div class="blog__item__text">
                                            <ul>
                                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                                            </ul>
                                            <h5><a href="#">Gender-based violence (GBV)</a></h5>
                                            <p>GBV is violence directed against a person because of that person's gender or...</p>
                                            <a href="/client-news-details" class="blog__btn">READ MORE <span class="arrow_right"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection

@section('scripts')
@endsection
