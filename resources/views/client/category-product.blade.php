@extends('layouts.client')

@section('styles')
@endsection

@section('content')
    <main role="main" class="main-content">
        <div>
            @livewire('category-products', ['category'=>$category])
        </div>
    </main>
@endsection

@section('scripts')
@endsection
