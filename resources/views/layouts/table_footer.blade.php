<div class="row">
    <div class="col-md-8">
        {{ $results->links() }}
    </div>
    <div class="col-md-4">
         <span class="ml-5 mr-2">
            Showing {{ $results->firstItem() }} to {{ $results->lastItem() }} of {{ $results->total() }} entries
         </span>
    </div>
</div>

