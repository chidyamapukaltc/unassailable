<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Unassailable</title>
    <!-- Simple bar CSS -->


    <link rel="stylesheet" href="{{ asset('css/simplebar.css') }}">
    <!-- Fonts CSS -->
    <link
        href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,100;0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <!-- Icons CSS -->
    <link rel="icon" href="{{ asset('client/img/logo.jpg') }}" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset('client/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/elegant-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/slicknav.min.css') }}">
    <link rel="stylesheet" href="{{ asset('client/css/style.css') }}">


{{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
{{--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>--}}

    <!-- Date Range Picker CSS -->
    <link rel="stylesheet" href="{{ asset('client/css/owl.carousel.min.css') }}">
    <!-- App CSS -->

    {{--    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />--}}

    {{--    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
    {{--    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>--}}

    <style type="text/css" media="print">
        div.from {
            float: left;
        }

    </style>
    @livewireStyles()
    <style>

        #Iframe {
            width: 1200px;
            height: 1040px;
            resize: both;
        }
    </style>
    @yield('styles')
</head>
<body style="background-color:  #f0e5d6">
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<div>
    <!-- Humberger Begin -->
    <div class="humberger__menu__overlay"></div>
    <div class="humberger__menu__wrapper">
        <div class="header__top__right__auth">
               <a href="/" class="nav-link px-0">Home</a>
               <a href="{{route('client.products')}}" class="nav-link px-0">Products</a>
                <a href="{{route('client.awareness')}}" class="nav-link px-0">Awareness</a>
                <a href="{{route('client.contact')}}"class="nav-link px-0">Contact</a>

        </div>


    </div>

    <div id="mobile-menu-wrap"></div>

</div>
    <!-- Humberger End -->
{{--style="background: #dcc19c"--}}

    <!-- Header Section Begin -->

    <header class="header" style="background: #dcc19c">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="/"><img src="{{ asset('client/img/logo.jpg') }}"  height="54" width="54" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="header__menu">
                        <ul>
                            <li class="active"><a href="/">Home</a></li>
                            <li><a href="{{route('client.products')}}">Products</a></li>
                            <li><a href="{{route('client.awareness')}}">Awareness</a></li>
                            <li><a href="{{route('client.contact')}}">Contact</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3">
                    <div class="header__cart">
                        <ul>
{{--                            <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>--}}
                            <li><a href="{{route('client.cart')}}"><i class="fa fa-shopping-bag"></i> <span></span></a></li>
                        </ul>
                        <div class="header__top__right__auth">
                            <a href="/login"><i class="fa fa-user"></i> Login</a>
                        </div>
                        <div class="header__top__right__auth">
                        <a  href="/logout"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">| Logout</a>
                        <form id="logout-form" action="" method="POST">
                            @csrf
                        </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>


    @yield('content')
    <footer class="footer spad" style="background: #dcc19c">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__about__logo">
                            <a href="/"><img src="img/logo.png" alt=""></a>
                        </div>
                        <ul>
                            <li>Address: 15 Trail Rd Mt Pleasant</li>
                            <li>Phone: +26377 281 7517</li>
                            <li>Email: info@unassailable.co.zw</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                    <div class="footer__widget">
                        <h6>Useful Links</h6>
                        <ul>
                            <li><a href="{{route('client.products')}}">Products</a></li>
                            <li><a href="{{route('client.awareness')}}">Awareness</a></li>
                            <li><a href="{{route('client.news')}}">Featured Articles</a></li>
                            <li><a href="{{route('client.faq')}}">FAQ</a></li>


                        </ul>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="{{route('client.contact')}}">Contact</a></li>
                            <li><a href="#">Delivery information</a></li>
                            <li><a href="#">Privacy Policy</a></li>

                        </ul>

                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="footer__widget">
                        <h6>Join Our Newsletter Now</h6>
                        <p>Get E-mail updates about our latest shop and special offers.</p>
                        <form action="{{route('news.subscription')}}">
                            <input type="text" name="email" required placeholder="Enter your mail">
                            <button type="submit" class="site-btn">Subscribe</button>
                        </form>
                        <div class="footer__widget__social">
                            <a href="https://www.facebook.com/profile.php?id=100069399931165&mibextid=LQQJ4d"><i class="fa fa-facebook"></i></a>
                            <a href="https://instagram.com/unassailable_zw?igshid=YmMyMTA2M2Y="><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="https://wa.me/263772817517"><i class="fa fa-whatsapp"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__copyright">
                        <div class="footer__copyright__text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved @unassailable</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                        <div class="footer__copyright__payment"><img src="{{ asset('client/img/payment-item.png') }}" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

</div> <!-- .wrapper -->

@livewireScripts()
<script src="{{ asset('client/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('client/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('client/js/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('client/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('client/js/jquery.slicknav.js') }}"></script>
<script src='{{ asset('client/js/mixitup.min.js') }}'></script>
<script src='{{ asset('client/js/owl.carousel.min.js') }}'></script>
<script src="{{ asset('client/js/main.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script
    type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/5.0.0/mdb.min.js"
></script>
<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top',
        showConfirmButton: false,
        showCloseButton: true,
        timer: 10000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    window.addEventListener('alert', ({detail: {type, message}}) => {
        Toast.fire({
            icon: type,
            title: message
        })
    })
</script>
<script src="{{asset('js/countdown.js')}}"></script>
<script>
  (function($) {

        var MERCADO_JS = {
            init: function(){
                this.mercado_countdown();

            },
            mercado_countdown: function() {
                if($(".mercado-countdown").length > 0){
                    $(".mercado-countdown").each( function(){
                        var _this = $(this),
                            _expire = _this.data('expire');
                        _this.countdown(_expire, function(event) {
                            $(this).html( event.strftime('<span><b>%D</b> Days</span> <span><b>%-H</b> Hrs</span> <span><b>%M</b> Mins</span> <span><b>%S</b> Secs</span>'));
                        });
                    });
                }
            },

        }

        window.onload = function () {
            MERCADO_JS.init();
        }

    })(window.Zepto || window.jQuery, window, document);
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/6385e861b0d6371309d1b1a8/1gj1h7gcf';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
@stack('scripts')
<script src="{{ asset('js/apps.js') }}"></script>
@yield('scripts')
</body>
</html>
