<div class="row mb-4 items-align-center">
    <div class="col-md">

        <form class="form-inline">
            <div class="form-row">
                <div class="form-group col-auto mr-auto">
                    <span class="text-muted">Rows Per Page :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <select class="custom-select mr-sm-2"  wire:model="paginationCount" id="inlineFormCustomSelectPref1">
                        <option selected="15" value="10">10</option>
                        <option  value="15">15</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="50">50</option>
                        <option value="10">100</option>
                    </select>
                </div>

                <div class="form-group col-auto ml-3">
                    <span class="text-muted">Sort By :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <select class="custom-select my-1 mr-sm-2"  wire:model="orderBy" id="inlineFormCustomSelectPref">
                        @foreach($columns as $columnName => $column)
                            <option value="{{ $columnName  }}">{{ $columnName }}</option>
                        @endforeach


                    </select>
                </div>

                <div class="form-group col-auto ml-3">
                    <span class="text-muted">Rows Per Page :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <select class="custom-select my-1 mr-sm-2"  wire:model="orderDirection" id="inlineFormCustomSelectPref">
                        <option value="asc">ASC</option>
                        <option value="desc">DESC</option>
                    </select>
                </div>
                <div class="form-group col-auto">
                    <span class="text-muted">Search :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <input type="text" class="form-control" id="search" wire:model="search" placeholder="Search">
                </div>

            </div>
        </form>



{{--        <ul class="nav nav-pills justify-content-start">--}}
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link active bg-transparent pr-2 pl-0 text-primary" href="#">All <span class="badge badge-pill bg-primary text-white ml-2">164</span></a>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link text-muted px-2" href="#">Active <span class="badge badge-pill bg-white border text-muted ml-2">64</span></a>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link text-muted px-2" href="#">Renewal <span class="badge badge-pill bg-white border text-muted ml-2">48</span></a>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link text-muted px-2" href="#">Paid <span class="badge badge-pill bg-white border text-muted ml-2">52</span></a>--}}
{{--            </li>--}}
{{--        </ul>--}}
    </div>
{{--            <div class="col-md-auto ml-auto text-right">--}}
{{--                      <span class="small bg-white border py-1 px-2 rounded mr-2 d-none d-lg-inline">--}}
{{--                        <a href="#" class="text-muted"><i class="fe fe-x mx-1"></i></a>--}}
{{--                        <span class="text-muted">Status : <strong>Pending</strong></span>--}}
{{--                      </span>--}}
{{--                <span class="small bg-white border py-1 px-2 rounded mr-2 d-none d-lg-inline">--}}
{{--                        <a href="#" class="text-muted"><i class="fe fe-x mx-1"></i></a>--}}
{{--                        <span class="text-muted">April 14, 2020 - May 13, 2020</span>--}}
{{--                      </span>--}}
{{--            </div>--}}
    @if($showCreate)
{{--        <div class="col-auto">--}}
{{--            <button type="button" style="float: right;" class="btn btn-success" data-toggle="modal" data-target="#exampleModal"><span class="fe fe-filter fe-12 mr-2"></span>Report</button>--}}
{{--        </div>--}}
    <div class="col-auto">
        <button type="submit" wire:click="{{$createFunction}}" class="btn btn-primary"><span class="fe fe-filter fe-12 mr-2"></span>Create</button>
    </div>
    @endif
</div>

