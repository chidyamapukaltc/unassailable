<div>
    {{-- The whole world belongs to you. --}}
    <!-- Hero Section Begin -->
    <section class="hero"  >
        <div class="cont" >
{{--            <div class="row">--}}
                {{--                <div class="col-lg-3">--}}
                {{--                    <div class="hero__categories">--}}
                {{--                        <div class="hero__categories__all">--}}
                {{--                            <i class="fa fa-bars"></i>--}}
                {{--                            <span>All Categories</span>--}}
                {{--                        </div>--}}
                {{--                        <ul>--}}
                {{--                            @foreach($subCategories as $subCategory)--}}
                {{--                            <li><a href="#" >{{$subCategory->name}}</a></li>--}}
                {{--                            @endforeach--}}

                {{--                        </ul>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
{{--                <div class="col-lg-12">--}}
{{--                    <div class="hero__search">--}}
{{--                                                <div class="hero__search__form">--}}
{{--                                                    <form action="#">--}}
{{--                                                        <input type="text" placeholder="What do yo u need?">--}}
{{--                                                        <button type="button"    class="site-btn">SEARCH</button>--}}
{{--                                                    </form>--}}
{{--                                                </div>--}}


{{--                    </div>--}}
                    <div class="hero__item set-bg" data-setbg="client/img/logo.png">
                        <div class="hero__text1">
                            <h2>Unassailable</h2>
                            <p>Self preservation at all cost</p>

                        </div>
                    </div>
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Categories Section Begin -->
    {{--    <section class="categories">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row">--}}
    {{--                <div class="categories__slider owl-carousel">--}}
    {{--                    @foreach($auctions as $auction)--}}
    {{--                    <div class="col-lg-3">--}}
    {{--                        @if($auction->auctionImage)--}}
    {{--                        <div class="categories__item set-bg" data-setbg="{{url('storage/'.$auction->auctionImage->path)}}">--}}
    {{--                            <h5><a href="{{route('client.auction.product',[$auction->id])}}">{{$auction->name}}</a></h5>--}}
    {{--                        </div>--}}
    {{--                        @else--}}
    {{--                            <div class="categories__item set-bg" data-setbg="client/img/product/details/product-details-1.jpg">--}}
    {{--                                <h5><a href="{{route('client.auction.product',[$auction->id])}}">{{$auction->name}}</a></h5>--}}
    {{--                            </div>--}}
    {{--                        @endif--}}
    {{--                    </div>--}}
    {{--                    @endforeach--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}
    <!-- Categories Section End -->
    <!-- Featured Section Begin -->

    <section class="featured spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Featured Products</h2>
                    </div>
{{--                                        <div class="featured__controls">--}}
{{--                                            <ul>--}}
{{--                                                <li class="active"  data-filter=".all">All</li>--}}
{{--                                                <li   data-filter=".surveillance">Surveillance</li>--}}
{{--                                                <li data-filter=".security">Money Security</li>--}}
{{--                                                <li   data-filter=".defence">Women Defence</li>--}}
{{--                                                <li data-filter=".gadgets">Security Gadgets</li>--}}

{{--                                            </ul>--}}
{{--                                         </div>--}}
                </div>
            </div>

{{--            <div class="row featured__filter">--}}

{{--                @foreach($results as $product)--}}
{{--                    <div class="col-lg-3 col-md-4 col-sm-6 mix all {{$product->category}}">--}}
{{--                        <div class="featured__item">--}}
{{--                            @if(!empty($product->productImage[0]))--}}

{{--                                <div class="featured__item__pic set-bg" data-setbg="{{url('storage/'.$product->productImage[0]->path)}}">--}}
{{--                                    <ul class="featured__item__pic__hover">--}}
{{--                                        <li><a href="#"><i class="fa fa-heart icon-active"></i></a></li>--}}
{{--                                        --}}{{--                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>--}}
{{--                                        --}}{{--                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            @else--}}
{{--                                <div class="featured__item__pic set-bg" data-setbg="{{url('client/img/product/product-1.jpg')}}">--}}
{{--                                    <ul class="featured__item__pic__hover">--}}
{{--                                        <li><a href="#"><i class="fa fa-heart icon-active"></i></a></li>--}}
{{--                                        --}}{{--                                <li><a href="#"><i class="fa fa-v  retweet"></i></a></li>--}}
{{--                                        --}}{{--                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            @endif--}}
{{--                            <div class="featured__item__text">--}}
{{--                                <h6 ><a href="{{route('client.product.details',$product->id)}}">{{$product->name}}</a></h6>--}}
{{--                                <h5>${{$product->price}}</h5>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}

{{--            </div>--}}


        </div>
    </section>
        <div class="cont">

            <div class="card">
                <a href="/category-products/surveillance">
                <div class="face face2">

                    <h2>Surveillance</h2>
                </div>
                </a>
            </div>

            <div class="card">
                <a href="/category-products/security">
                <div class="face face2">
                    <h2>Money Security</h2>
                </div>
                </a>
            </div>

            <div class="card">
                <a href="/category-products/defence">
                <div class="face face2">
                    <h2>Women Defence</h2>
                </div>
                </a>
            </div>
            <div class="card">
                <a href="/category-products/gadgets">
                <div class="face face2">
                    <h2>Security Gadgets</h2>
                </div>
                </a>
            </div>
        </div>


    <!-- Featured Section End -->
    <section class="from-blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title from-blog__title">
                        <h2 >Awareness</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog__item">
                        <div class="blog__item__pic">
                            <img src="{{url('assets/images/gbv.jpeg')}}" alt="">
                        </div>
                        <div class="blog__item__text">
                            <ul>
                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
{{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                            </ul>
                            <h5><a href="{{route('client.gbv')}}" >Gender-based violence (GBV)</a></h5>
                            <p>GBV is violence directed against a person because of that person's gender or... </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog__item">
                        <div class="blog__item__pic">
                            <img src="{{url('assets/images/anxiety2.jpeg')}}" height="224" alt="">
                        </div>
                        <div class="blog__item__text">
                            <ul>
                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                            </ul>
                            <h5><a href="{{route('client.anxiety')}}"  >Anxiety</a></h5>
                            <p>Anxiety is a feeling of fear, dread, and uneasiness. It might cause you to sweat... </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog__item">
                        <div class="blog__item__pic">
                            <img src="{{url('assets/images/depression.jpeg')}}" height="224" alt="">
                        </div>
                        <div class="blog__item__text">
                            <ul>
                                <li><i class="fa fa-calendar-o"></i> Nov 26,2022</li>
                                {{--                                <li><i class="fa fa-comment-o"></i> 5</li>--}}
                            </ul>
                            <h5><a href="{{route('client.depression')}}"  >Depression</a></h5>
                            <p>Depression is a mood disorder that causes feelings of sadness that won't go away....
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
