<div>
    {{-- Stop trying to control. --}}


    {{-- Do your work, then step back. --}}
{{--    <section class="hero">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}

{{--                <div class="col-lg-12">--}}
{{--                    <div class="hero__search">--}}
{{--                        <div class="hero__search__form">--}}
{{--                            <form action="#">--}}
{{--                                <input type="text" placeholder="What do yo u need?">--}}
{{--                                <button type="submit" class="site-btn">SEARCH</button>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                        <div class="hero__search__phone">--}}
{{--                            <div class="hero__search__phone__icon">--}}
{{--                                <i class="fa fa-phone"></i>--}}
{{--                            </div>--}}
{{--                            <div class="hero__search__phone__text">--}}
{{--                                <h5>+263 719 999 999</h5>--}}
{{--                                <span>support 24/7 time</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="row" style="margin-top: 15px">--}}
{{--                        @foreach($products as $product)--}}
{{--                            <div class="col-lg-4 col-md-6 col-sm-6">--}}
{{--                                <div class="product__item">--}}
{{--                                    <div class="product__item__pic set-bg" >--}}
{{--                                        @if(!empty($product->productImage[0]))--}}
{{--                                            <img src="{{url('storage/'.$product->productImage[0]->path)}}" height="250" width="190">--}}
{{--                                        @else--}}
{{--                                            <img src="{{url('client/img/product/product-1.jpg')}}"  height="250" width="190">--}}
{{--                                        @endif--}}

{{--                                    </div>--}}
{{--                                    <div class="product__item__text">--}}
{{--                                        <h6><a href="{{route('client.product.details',$product->id)}}" >{{$product->name}}</a></h6>--}}
{{--                                        <h5 style="color: red">${{$product->price }} </h5>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                            @foreach($products as $product)--}}
{{--                                <div class="col-lg-3 col-md-4 col-sm-6 ">--}}
{{--                                    <div class="featured__item">--}}
{{--                                        @if(!empty($product->productImage[0]))--}}

{{--                                            <div class="featured__item__pic set-bg" data-setbg="{{url('storage/'.$product->productImage[0]->path)}}">--}}
{{--                                                <ul class="featured__item__pic__hover">--}}
{{--                                                    --}}{{--                                        <li><a href="#"><i class="fa fa-heart icon-active"></i></a></li>--}}
{{--                                                    --}}{{--                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>--}}
{{--                                                    --}}{{--                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        @else--}}
{{--                                            <div class="featured__item__pic set-bg" data-setbg="{{url('client/img/product/product-1.jpg')}}">--}}
{{--                                                <ul class="featured__item__pic__hover">--}}
{{--                                                    --}}{{--                                        <li><a href="#"><i class="fa fa-heart icon-active"></i></a></li>--}}
{{--                                                    --}}{{--                                <li><a href="#"><i class="fa fa-v  retweet"></i></a></li>--}}
{{--                                                    --}}{{--                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        @endif--}}
{{--                                        <div class="featured__item__text">--}}
{{--                                            <h6 ><a href="{{route('client.product.details',$product->id)}}">{{$product->name}}</a></h6>--}}
{{--                                            <h5 style="color: red">${{$product->price}}</h5>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--    </section>--}}
    <section class="featured spad">
        <div class="container">
            <div class="row">
{{--                <div class="col-lg-12">--}}
{{--                    <div class="section-title">--}}
{{--                        <h2>Featured Products</h2>--}}
{{--                    </div>--}}
{{--                    <div class="featured__controls">--}}
{{--                        <ul>--}}
{{--                            <li  class="active" data-filter=".all">All</li>--}}
{{--                            <li   data-filter=".surveillance" >Surveillance</li>--}}
{{--                            <li data-filter=".security">Money Security</li>--}}
{{--                            <li   data-filter=".defence" >Women Defence</li>--}}
{{--                            <li data-filter=".gadgets">Security Gadgets</li>--}}

{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>

            <div class="row featured__filter">

                @foreach($products as $product)
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="featured__item">
                            @if(!empty($product->productImage[0]))

                                <div class="featured__item__pic set-bg" data-setbg="{{url('storage/'.$product->productImage[0]->path)}}">
                                    <ul class="featured__item__pic__hover">
                                        {{--                                        <li><a href="#"><i class="fa fa-heart icon-active"></i></a></li>--}}
                                        {{--                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>--}}
                                        {{--                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>--}}
                                    </ul>
                                </div>
                            @else
                                <div class="featured__item__pic set-bg" data-setbg="{{url('client/img/product/product-1.jpg')}}">
                                    <ul class="featured__item__pic__hover">
                                        {{--                                        <li><a href="#"><i class="fa fa-heart icon-active"></i></a></li>--}}
                                        {{--                                <li><a href="#"><i class="fa fa-v  retweet"></i></a></li>--}}
                                        {{--                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>--}}
                                    </ul>
                                </div>
                            @endif
                            <div class="featured__item__text">
                                <h6 ><a href="{{route('client.product.details',$product->id)}}">{{$product->name}}</a></h6>
                                <h5 >${{$product->price}}</h5>
                            </div>
                        </div>
                    </div>
                @endforeach



            </div>
            <div class="row">
                <div class="col-md-8">
                    {{ $products->links() }}
                </div>
                <div class="col-md-4">

            Showing {{ $products->firstItem() }} to {{ $products->lastItem() }} of {{ $products->total() }} entries
         </span>
                </div>
            </div>


        </div>
    </section>
</div>

