<div>
    {{-- Knowing others is intelligence; knowing yourself is true wisdom. --}}
    @if($updateMode)
        @include('livewire.product.update-product')
    @elseif($createMode)
        @include('livewire.product.create-product')
    @else
        @include('livewire.product.show-product')
    @endif
</div>
