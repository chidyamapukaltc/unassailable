<div>
    <h2 class="h3 mb-3 page-title">Product</h2>
    @include('layouts.table_header')
    <div class="row">
        @foreach ($results as $item)
            <div class="col-md-4  mb-4">
                <div class="card profile shadow">
                    <div class="card-body ">
                        <div class="row align-items-center">
                            <div class="col-md-3 text-center ">
                                @if(!empty($item->productImage[0]))
                                        <a  class="avatar avatar-xl">
                                            <img src="{{url('storage/'.$item->productImage[0]->path)}}" alt="..." height="110" width="110" class="card-img img-fluid rounded">
                                        </a>
                                    @else
                                        <a  class="avatar avatar-xl">
                                            <img src="{{ asset('assets/images/meal.jpeg') }}" alt="..." class="card-img img-fluid rounded">
                                        </a>
                                    @endif

                            </div>
                            <div class="col">
                                <div class="row align-items-center">
                                    <div class="col-md-7">
                                        <h4> {{$item->name}}</h4>
                                    </div>

                                    <div class="col-md-7">
                                        <p class="text-muted"> {{$item->description}} </p>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-md-5 ">
                                        <span class="small text-muted mb-0"><span class="text-success">{{$item->price}} </span></span>
                                    </div>
                                    <div class="col ">
                                        <button type="button" class="btn btn-sm btn-danger" wire:click="destroy({{$item->id}})">Delete</button>
                                        <button type="button" class="btn btn-sm btn-warning" wire:click="edit({{$item->id}})" >Edit</button>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- / .row- -->
                    </div> <!-- / .card-body - -->
                </div> <!-- / .card- -->
            </div>
        @endforeach
    </div>
    <!-- Slide Modal -->
    @include('layouts.table_footer')
</div>
