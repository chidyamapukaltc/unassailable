+<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h2 class="page-title">Product</h2>

            <div class="row">
                <div class="col-md-12">
                    <div class="card shadow mb-4">
                        <div class="card-header">
                            <strong class="card-title">Product</strong>

                        </div>
                        <form>
                            <div class="col-md-12 my-4">
                                <div class="card shadow">
                                    <div class="card-body">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="inputPassword4">Name</label>
                                                <input type="text" class="form-control"  wire:model="name"  >
                                                @error('name') <span class="text-danger error">{{ $message }}</span>@enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="inputPassword4">Description</label>
                                                <input type="text" class="form-control" id="inputPassword4" wire:model="description"  >
                                                @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="inputPassword4">Product Information</label>
                                                <textarea type="text" class="form-control"  wire:model="information"></textarea>
                                                @error('information') <span class="text-danger error">{{ $message }}</span>@enderror
                                            </div>

                                        </div>


                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="inputState">Category</label>
                                                <select wire:model="category"   class="form-control">
                                                    <option selected value="None">Choose...</option>
                                                    <option  value="surveillance">Surveillance</option>
                                                    <option  value="security">Money Security</option>
                                                    <option  value="defence">Women Defence</option>
                                                    <option  value="gadgets">Security Gadgets</option>
                                                </select>
                                                @error('type') <span class="text-danger error">{{ $message }}</span>@enderror
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label for="inputState">Currency</label>
                                                <select wire:model="currency"   class="form-control">
                                                    <option selected value="None">Choose...</option>
                                                    <option  value="Auction">USD</option>
                                                    <option  value="For Sale">ZWL</option>
                                                </select>
                                                @error('type') <span class="text-danger error">{{ $message }}</span>@enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="inputPassword4">Price</label>
                                                <input type="text" class="form-control"  wire:model="price"  >
                                                @error('price') <span class="text-danger error">{{ $message }}</span>@enderror
                                            </div>
                                        </div>


{{--                                        <div class="col-auto">--}}
{{--                                            <button type="submit" style="float: right; margin-bottom: 10px;" wire:click.prevent="add({{$i}})" class="btn btn-success"><span class="fe fe-plus fe-12 mr-2"></span> Add</button>--}}

{{--                                        </div>--}}
{{--                                        <h5 class="card-title">Images</h5>--}}


{{--                                        <p class="card-text"></p>--}}

{{--                                        <table class="table table-striped table-hover">--}}
{{--                                            <thead>--}}
{{--                                            <tr>--}}
{{--                                                <th>Image Name</th>--}}
{{--                                                <th>Image</th>--}}
{{--                                                <th>Action</th>--}}
{{--                                            </tr>--}}
{{--                                            </thead>--}}
{{--                                            <tbody>--}}
{{--                                            <tr>--}}
{{--                                                <td>--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <input type="text" class="form-control" wire:model="imageName.0" >--}}
{{--                                                        @error('imageName.0') <span class="text-danger error">{{ $message }}</span>@enderror--}}
{{--                                                    </div>--}}

{{--                                                </td>--}}

{{--                                                <td>--}}
{{--                                                    <div class="custom-file">--}}
{{--                                                        <input type="file" wire:model="imagePath.0" class="form-control-file">--}}
{{--                                                        @error('imagePath.0') <span class="text-danger error">{{ $message }}</span>@enderror--}}
{{--                                                    </div>--}}

{{--                                                </td>--}}


{{--                                            </tr>--}}
{{--                                            @foreach($inputs as $key => $value)--}}
{{--                                                <tr>--}}
{{--                                                    <td>--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <input type="text" class="form-control" wire:model="imageName.{{$value}}" >--}}
{{--                                                            @error('imageName.'.$value) <span class="text-danger error">{{ $message }}</span>@enderror--}}
{{--                                                        </div>--}}

{{--                                                    </td>--}}


{{--                                                    <td>--}}
{{--                                                        <div class="custom-file">--}}
{{--                                                            <input type="file" wire:model="imagePath.{{$value}}" class="form-control-file">--}}
{{--                                                            @error('imagePath.'.$value) <span class="text-danger error">{{ $message }}</span>@enderror--}}
{{--                                                        </div>--}}

{{--                                                    </td>--}}

{{--                                                    <td>--}}
{{--                                                        <button type="button" style="float: right;" wire:click.prevent="remove({{$key}})" class="btn btn-danger" ><span class="fe fe-minus fe-12 mr-2">Remove</span></button>--}}

{{--                                                    </td>--}}
{{--                                                </tr>--}}
{{--                                            @endforeach--}}

{{--                                            </tbody>--}}
{{--                                        </table>--}}
                                    </div>
                                </div>

                                <div class="form-group">

                                </div>


                                <button type="button" wire:click="update()" class="btn btn-primary">Update</button>
                            </div>

                        </form>
                    </div> <!-- /. card -->
                </div> <!-- /. col -->
            </div> <!-- /. end-section -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->
