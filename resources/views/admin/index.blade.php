@extends('layouts.master')

@section('styles')
@endsection

@section('content')
    <main role="main" class="main-content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="row align-items-center mb-2">
                        <div class="col">
                            <h2 class="h5 page-title">Orders</h2>
                        </div>

                    </div>

                    <!-- charts-->
                    <div>
                        @livewire('admin-orders')
                    </div>


                    {{--                    <div class="row my-4">--}}

                    {{--                        <div class="col-md-12 mb-4">--}}
                    {{--                            <div class="card shadow">--}}
                    {{--                                <div class="card-header">--}}
                    {{--                                    <strong class="card-title mb-0">Line Chart</strong>--}}
                    {{--                                    <span class="badge badge-light float-right mr-2">30 days</span>--}}
                    {{--                                    <span class="badge badge-light float-right mr-2">7 days</span>--}}
                    {{--                                    <span class="badge badge-secondary float-right mr-2">Today</span>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="card-body"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>--}}
                    {{--                                    <canvas id="lineChartjs" width="503" height="300" class="chartjs-render-monitor" style="display: block; width: 503px; height: 300px;"></canvas>--}}
                    {{--                                </div> <!-- /.card-body -->--}}
                    {{--                            </div> <!-- /.card -->--}}
                    {{--                        </div> <!-- /. col -->--}}
                    {{--                    </div>--}}
                    <!-- info small box -->
                    {{--                    <div class="row">--}}
                    {{--                        <!-- Recent orders -->--}}
                    {{--                        <div class="col-md-12">--}}
                    {{--                            <h6 class="mb-3">Summary</h6>--}}
                    {{--                            <table class="table table-borderless table-striped">--}}
                    {{--                                <thead>--}}
                    {{--                                <tr role="row">--}}
                    {{--                                    <th>Tables</th>--}}
                    {{--                                    <th>Menus </th>--}}
                    {{--                                    <th>Meals</th>--}}
                    {{--                                    <th>Approved Orders</th>--}}
                    {{--                                    <th>Completed Orders</th>--}}
                    {{--                                </tr>--}}
                    {{--                                </thead>--}}
                    {{--                                <tbody>--}}
                    {{--                                <tr>--}}
                    {{--                                    <th scope="col">Gauteng</th>--}}
                    {{--                                    <td>20</td>--}}
                    {{--                                    <td>10</td>--}}
                    {{--                                    <td>100</td>--}}
                    {{--                                    <td>200</td>--}}
                    {{--                                    <td>1000</td>--}}

                    {{--                                </tr>--}}
                    {{--                                <tr>--}}
                    {{--                                    <th scope="col">Gauteng</th>--}}
                    {{--                                    <td>20</td>--}}
                    {{--                                    <td>10</td>--}}
                    {{--                                    <td>100</td>--}}
                    {{--                                    <td>200</td>--}}
                    {{--                                    <td>1000</td>--}}

                    {{--                                </tr>--}}
                    {{--                                <tr>--}}
                    {{--                                    <th scope="col">Gauteng</th>--}}
                    {{--                                    <td>20</td>--}}
                    {{--                                    <td>10</td>--}}
                    {{--                                    <td>100</td>--}}
                    {{--                                    <td>200</td>--}}
                    {{--                                    <td>1000</td>--}}

                    {{--                                </tr>--}}
                    {{--                                <tr>--}}
                    {{--                                    <th scope="col">Gauteng</th>--}}
                    {{--                                    <td>20</td>--}}
                    {{--                                    <td>10</td>--}}
                    {{--                                    <td>100</td>--}}
                    {{--                                    <td>200</td>--}}
                    {{--                                    <td>1000</td>--}}
                    {{--                                </tr>--}}
                    {{--                                <tr>--}}
                    {{--                                    <th scope="col">Gauteng</th>--}}
                    {{--                                    <td>20</td>--}}
                    {{--                                    <td>10</td>--}}
                    {{--                                    <td>100</td>--}}
                    {{--                                    <td>200</td>--}}
                    {{--                                    <td>1000</td>--}}

                    {{--                                </tr>--}}
                    {{--                                <tr>--}}
                    {{--                                    <th scope="col">Gauteng</th>--}}
                    {{--                                    <td>20</td>--}}
                    {{--                                    <td>10</td>--}}
                    {{--                                    <td>100</td>--}}
                    {{--                                    <td>200</td>--}}
                    {{--                                    <td>1000</td>--}}

                    {{--                                </tr>--}}
                    {{--                                <tr>--}}
                    {{--                                    <th scope="col">Gauteng</th>--}}
                    {{--                                    <td>20</td>--}}
                    {{--                                    <td>10</td>--}}
                    {{--                                    <td>100</td>--}}
                    {{--                                    <td>200</td>--}}
                    {{--                                    <td>1000</td>--}}

                    {{--                                </tr>--}}

                    {{--                                </tbody>--}}
                    {{--                            </table>--}}
                    {{--                        </div> <!-- / .col-md-3 -->--}}
                    {{--                    </div> <!-- end section -->--}}
                </div>
            </div> <!-- .row -->
        </div> <!-- .container-fluid -->


    </main>
@endsection

@section('scripts')

    <script>

        const ctx = document.getElementById('myChart').getContext('2d');
        const myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                datasets: [{
                    label: 'Orders',
                    data: [1, 2, 3, 2, 3, 4, 2, 4, 5, 2, 5, 4],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                    ],
                    // borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
    <script>



        const ctxp = document.getElementById('pieChartOrder').getContext('2d');
        const pieChartOrder = new Chart(ctxp, {
            type: 'pie',
            data: {
                labels: ['Cancelled', 'Pending', 'Completed'],
                datasets: [{
                    label: 'Orders',
                    data: [1, 2, 3],
                    backgroundColor: [
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)',
                        'rgb(56, 142, 60)'
                    ],
                    // borderColor: [
                    //     'rgba(255, 99, 132, 1)',
                    //     'rgba(54, 162, 235, 1)',
                    //     'rgba(255, 206, 86, 1)',
                    //     'rgba(75, 192, 192, 1)',
                    //     'rgba(153, 102, 255, 1)',
                    //     'rgba(255, 159, 64, 1)'
                    // ],
                    // borderWidth: 1
                    hoverOffset: 4
                }]
            },
        });
    </script>

@endsection
