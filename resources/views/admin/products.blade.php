@extends('layouts.master')

@section('styles')
@endsection

@section('content')
    <main role="main" class="main-content">
        <div>
            @livewire('admin-products')
        </div>
    </main>
@endsection

@section('scripts')
@endsection
