<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login - Unassailable</title>
    <!-- Simple bar CSS -->
    <link rel="stylesheet" href="{{ asset('css/simplebar.css') }}">
    <link rel="icon" href="{{ asset('assets/img/logo.jpg') }}" type="image/x-icon">
    <!-- Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,100;0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Icons CSS -->
    <link rel="stylesheet" href="{{ asset('css/feather.css') }}">
    <!-- Date Range Picker CSS -->
    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">
    <!-- App CSS -->
    <link rel="stylesheet" href="{{ asset('css/app-light.css') }}" id="lightTheme">
</head>
<body class="light ">
<div class="wrapper vh-100">
    <div class="row align-items-center h-100">

        <form class="col-lg-4 col-md-5 col-10 mx-auto text-center" action="{{ route('login') }}" method="POST">
            <div class="card shadow border-0">
                <div class="card-body">
                    @csrf

                    <a class="navbar-brand mx-auto mt-2 mb-4 flex-fill text-center" href="#">
                        <img src="{{ asset('client/img/logo.jpg') }}" alt="On Order" style="height: 50px;">
                    </a>

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{ Session::get('error')}}
                        </div>
                    @endif
                    <h1 class="h6 mb-3">Sign in</h1>
                    <div class="form-group">
                        <label for="email" class="sr-only">{{ __('Email address') }}</label>
                        <input type="email" id="email" name="email" class="form-control form-control-lg @error('email') is-invalid @enderror" placeholder="Email address" value="{{ old('email') }}" autocomplete="off" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password" class="sr-only">{{ __('Password') }}</label>
                        <input type="password" id="password" name="password" class="form-control form-control-lg @error('password') is-invalid @enderror" placeholder="Enter your password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                        @enderror
                    </div>
                    <div class="checkbox mb-3">
                        <input type="checkbox" value="remember-me" {{ old('remember') ? 'checked' : '' }}>Stay logged in
                    </div>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">{{ __('Login') }}</button>
                    <a class="btn btn-link" href="{{ route('register') }}">
                        {{ __('Create an account?') }}
                    </a>
                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                    <p class="mt-5 mb-3 text-muted">© <?php echo date('Y')?></p>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/simplebar.min.js') }}"></script>
<script src='{{ asset('js/daterangepicker.js') }}'></script>
<script src='{{ asset('js/jquery.stickOnScroll.js') }}'></script>
<script src="{{ asset('js/tinycolor-min.js') }}"></script>
<script src="{{ asset('js/config.js') }}"></script>
<script src="{{ asset('js/apps.js') }}"></script>
</body>
</html>
