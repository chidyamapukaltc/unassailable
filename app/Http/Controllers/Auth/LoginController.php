<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $input = $request->all();
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);


        if (auth()->attempt(array('email' => $input['email'], 'password' => $input['password']))) {
//            return redirect()->route('admin.index');
            if (Auth()->user()->role == 'Admin') {
                return redirect()->route('admin.index');
            } elseif  (Auth()->user()->role == 'Client'){
                return redirect()->route('client.index');
            } else {
                return route('no_role');
            }

        } else {
            return redirect()->route('login')->with('error', 'Your username and password combination is wrong');
        }

    }

    protected function redirectTo()
    {

        if (Auth()->user()->role == 'Admin') {
            return redirect()->route('admin.index');
        } elseif  (Auth()->user()->role == 'Client'){
            return redirect()->route('client.index');
        } else {
            return route('no_role');
        }
    }
}
