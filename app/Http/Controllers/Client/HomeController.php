<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Mail\ContactFormMail;
use App\Models\Contactus;
use App\Models\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{

    public function index()
    {
        return view('client.index');
    }

    public function clientNewsSubscription(Request $request)
    {
        $request->validate([
            'email' => 'required',

        ]);
        Subscription::create([
            'email' => $request->get('email')

        ]);

    }
    public function contactMail(Request $request){
        $contact = $request->validate( [
            'name' => ['required', 'string', 'max:255' ],
            'email' => ['required', 'string', 'email', 'max:255' ],
            'phone' => ['string', 'max:255'],
            'subject' => ['required', 'string', 'max:255'],
            'message' => ['required', 'string', 'max:255']
        ]);
        Contactus::create($contact);
        Mail::to('info@unassailable.co.zw')->send(new ContactFormMail($contact));
        return redirect('/#contact')->with('success', 'Message Sent');
    }

    public function clientProducts()
    {
        return view('client.client_products');
    }

    public function clientProductDetails($productId)
    {
        return view('client.client_product_details', compact('productId'));
    }
    public function categoryProduct($category)
    {
        return view('client.category-product', compact('category'));
    }


    public function clientNews()
    {
        return view('client.client_news');
    }

    public function clientCart()
    {
        return view('client.client_cart');
    }

    public function clientCheckout()
    {
        return view('client.client_checkout');
    }

    public function clientAwareness()
    {
        return view('client.client_awareness');
    }

    public function clientAwarenessGbv()
    {
        return view('client.gbv');
    }

    public function clientAwarenessAnxiety()
    {
        return view('client.anxiety');
    }

    public function clientAwarenessDepression()
    {
        return view('client.depression');
    }

    public function clientFaq()
    {

        return view('client.client_faq');
    }

    public function clientContact()
    {

        return view('client.contact');
    }

    public function clientNewsDetails()
    {

        return view('client.client_news_details');
    }

 }
