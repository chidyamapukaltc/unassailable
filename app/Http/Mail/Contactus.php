<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contactus extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->contactus = $data;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@unassailble.co.zw')
            ->markdown('template.contactform')
            ->with([
                'subject' => $this->contactus['subject'],
                'message' => $this->contactus['message'],
                'email' => $this->contactus['email'],
                'phone' => $this->contactus['phone'],
                'name' => $this->contactus['name'],
            ]);
    }
}
