<?php

namespace App\Http\Livewire;

use App\Http\Traits\IsTable;
use App\Models\Cart;
use App\Models\CartItem;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class ClientCart extends Component
{
    public $updateMode = false;
    public $createMode= false;
    public $viewMode = false;
    public $showCreate = true;
    public $inputs = [];
    public $i = 1;
    public $createFunction = 'create';
    use WithPagination,IsTable,WithFileUploads;
    protected $listeners = [
        'deleteCategory'=>'destroy'
    ];
    public $quantity =[];
    public $total;
    public function render()
    {
        if (!is_null(Auth::user())) {
            $cart = Cart::where('user_id', Auth::user()->id)->where('status', 'Pending')->first();

        if ($cart) {
            foreach ($cart->cartItem as $item) {
                $this->total = +$item->quantity * $item->product->price;
            }
        }
    }
        return view('livewire.client-cart',compact('cart'));
    }

    public function deleteCartItem($id){
        $cart = CartItem::where('id',$id)->first();
        $cart->delete();
    }

    public function checkout($id){
        $cart = Cart::where('id',$id)->where('status','Pending')->first();
        $cart->update([
//            'total'=>$this->total,
            'status'=>'Order'
        ]);
        foreach ($cart->cartItem as $item){
            $item->update([
                'status'=>'Order'
            ]);
        }
        return redirect()->to('/client-checkout');
    }

}
