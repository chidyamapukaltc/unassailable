<?php

namespace App\Http\Livewire;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ClientProductDetails extends Component
{
    public $productId;
    public $quantity = 1;
    public function mount($productId){
        $this->productId = $productId;
        if (!is_null(Auth::user())) {
            $cart = Cart::where('id', Auth::user()->id)->first();
            if($cart){
                $cartQuantity = $cart->cartItem->where('product_id', $this->productId)->first();
                if ($cartQuantity){
                    $this->quantity = $cartQuantity->quantity;
                }

            }


        }
    }
    public function render()
    {
        $product = Product::where('id',$this->productId)->first();

        return view('livewire.client-product-details', compact('product'));
    }

    public function addToCart(){
        if (is_null(Auth::user())){
            return redirect()->to('/login');
        }else{
            $cart = Cart::where('user_id', Auth::user()->id)->where('status', 'Pending')->first();
            if ($cart){
                $cartItem = CartItem::where('cart_id',$cart->id)->where('product_id',$this->productId)->first();
                if ($cartItem){
                    $cartItem->update([
                        'quantity'=>$this->quantity,
                         'status'=> 'Pending',
                    ]);
                    return redirect()->to('/client-cart');
                }else{
                    CartItem::create([
                        'product_id'=> $this->productId,
                        'cart_id'=> $cart->id,
                        'quantity'=>$this->quantity,
                        'status'=> 'Pending',
                    ]);
                    return redirect()->to('/client-cart');
                }

            }else{
                $product = Product::where('id',$this->productId)->first();
                $cartId = Cart::create([
                    'user_id'=> Auth::user()->id,
                    'status'=> 'Pending',

                ]);

                CartItem::create([
                    'product_id'=> $this->productId,
                    'cart_id'=> $cartId,
                    'quantity'=>$this->quantity,
                    'status'=> 'Pending',
                ]);
                return redirect()->to('/client-cart');
            }

        }
    }

}
