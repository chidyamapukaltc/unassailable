<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AdminOrders extends Component
{
    public function render()
    {
        return view('livewire.admin-orders');
    }
}
