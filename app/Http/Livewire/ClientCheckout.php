<?php

namespace App\Http\Livewire;

use App\Models\Cart;
use App\Models\Order;
use Exception;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ClientCheckout extends Component
{
    public $total,$firstname,$lastname,$phone,$email,$address,$city,$country;
    public function render()
    {
        if (!is_null(Auth::user())) {
            $cart = Cart::where('user_id', Auth::user()->id)->where('status', 'Order')->first();

            if ($cart) {
                foreach ($cart->cartItem as $item) {
                    $this->total = +$item->quantity * $item->product->price;
                }
            }
        }
        return view('livewire.client-checkout', compact('cart'));
    }

    public function placeOrder($id){

        try{
        Order::create([
        'firstname'=>$this->firstname,
         'lastname'=>$this->lastname,
            'phone'=>$this->phone,
            'email'=>$this->email,
            'address'=>$this->address,
            'city'=>$this->city,
            'country'=>$this->country,
            'status'=>'Pending',
            'user_id'=>Auth::user()->id,
            'cart_id'=>$id,
        ]);

            $this->dispatchBrowserEvent('alert',[
                'type'=>'success',
                'message'=>"Created Successfully!!"
            ]);
        return redirect()->to('/');

        }catch(Exception $e)
        {
            $this->dispatchBrowserEvent('alert',[
                'type'=>'error',
                'message'=>"Failed to create!!".$e
            ]);

        }
    }
}
