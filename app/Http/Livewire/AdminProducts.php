<?php

namespace App\Http\Livewire;

use App\Http\Traits\IsTable;
use App\Models\Product;
use App\Models\ProductImage;
use Exception;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class AdminProducts extends Component
{

    public $updateMode = false;
    public $createMode= false;
    public $viewMode = false;
    public $showCreate = true;
    public $inputs = [];
    public $i = 1;
    public $createFunction = 'create';
    use WithPagination,IsTable,WithFileUploads;
    protected $listeners = [
        'deleteCategory'=>'destroy'
    ];
    public  $name,$location, $description, $startDateTime,$endDateTime,$lotNumber,$quantity,$size, $type, $imageName,$information, $imagePath,
        $accessories,$category,$levy,$currency,$price, $currentPrice,$finalPrice,$auctionId,$subCategoryId;



    public function mount () {
        $this->columns = [
            'Name' => 'name',

            'Date' => 'created_at'
        ];
    }
    public function render()
    {
        $results = \App\Models\Product::where(function ($q){
            $q->orwhere('name','LIKE','%' . $this->search . '%')
                ->orwhere('category','LIKE','%' . $this->search .  '%')
                ->orwhere('description','LIKE','%' . $this->search .  '%');
        })
            ->orderBy($this->columns[$this->orderBy], $this->orderDirection)
            ->paginate($this->paginationCount);
        return view('livewire.admin-products',compact('results'));
    }
    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs ,$i);
    }

    public function remove($i)
    {
        unset($this->inputs[$i]);
    }

    public function create(){
        $this->createMode=true;
    }


    public function store()
    {
        try{



            $this->validate([
                'name' => 'required',
                'description' => 'required',
                'currency' => 'required',
                'price' => 'required',
                'category' => 'required',
                'information' => 'required',

            ]);



            $product = \App\Models\Product::create([
                'description' => $this->description,
                'information' => $this->information,
                'name' => $this->name,
                'currency' => $this->currency,
                'price' => $this->price,
                'category' => $this->category,

            ]);

            foreach ($this->imageName as $key => $value){
                if (!empty($this->imagePath[$key])){
                    $path = $this->imagePath[$key]->store('product', 'public');
                    ProductImage::create([
                        'product_id'=>$product->id,
                        'name' => $this->imageName[$key],
                        'path' => $path,
                    ]);
                }
            }

            $this->dispatchBrowserEvent('alert',[
                'type'=>'success',
                'message'=>"Created Successfully!!"
            ]);
            return $this->redirect('/admin/admin_products');

//            $this->createMode = false;

        }catch(Exception $e)
        {
            $this->dispatchBrowserEvent('alert',[
                'type'=>'error',
                'message'=>"Failed to create!!".$e
            ]);

        }

    }

    public function edit($id)
    {
        $record = \App\Models\Product::findOrFail($id);
        $this->selected_id = $id;
        $this->name = $record->name;
        $this->category = $record->category;
        $this->description = $record->description;
        $this->price = $record->price;
        $this->currency = $record->currency;
        $this->information = $record->information;
        $this->updateMode = true;
    }
    public function update()
    {
        try{

            $this->validate([
                'name' => 'required',
                'description' => 'required',
                'currency' => 'required',
                'price' => 'required',
                'category' => 'required',
                'information' => 'required',
            ]);

            if ($this->selected_id) {
                $record = \App\Models\Product::findorFail($this->selected_id);

                $record->update([
                    'description' => $this->description,
                    'name' => $this->name,
                    'currency' => $this->currency,
                    'price' => $this->price,
                    'category' => $this->category,
                    'information' => $this->information,
                ]);
//                $this->updateMode = false;
                $this->dispatchBrowserEvent('alert',[
                    'type'=>'success',
                    'message'=>"Updated Successfully!!"
                ]);
                return $this->redirect('/admin/admin_products');
            }
        }catch(Exception $e)
        {
            $this->dispatchBrowserEvent('alert',[
                'type'=>'error',
                'message'=>"Failed to create!!".$e
            ]);

        }
    }
    public function view($id)
    {
        $record = \App\Models\Product::findOrFail($id);

        $this->viewMode= true;



    }
    public function destroy($id)
    {
        $record = \App\Models\Product::findOrFail($id);
        $record->delete();
        $this->dispatchBrowserEvent('alert',[
            'type'=>'success',
            'message'=>"Deleted Successfully!!"
        ]);
    }
}
