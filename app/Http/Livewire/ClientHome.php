<?php

namespace App\Http\Livewire;

use App\Http\Traits\IsTable;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class ClientHome extends Component
{
    public $updateMode = false;
    public $createMode= false;
    public $viewMode = false;
    public $showCreate = true;
    public $inputs = [];
    public $i = 1;
    public $createFunction = 'create';
    use WithPagination,IsTable,WithFileUploads;
    protected $listeners = [
        'deleteCategory'=>'destroy'
    ];
    public function render()
    {
        $results = \App\Models\Product::where(function ($q){
            $q->orwhere('name','LIKE','%' . $this->search . '%')
                ->orwhere('currency','LIKE','%' . $this->search .  '%')
                ->orwhere('category','LIKE','%' . $this->search .  '%');
        })->orderBy($this->columns[$this->orderBy], $this->orderDirection)->get();
        return view('livewire.client-home',compact('results'));
    }
}
