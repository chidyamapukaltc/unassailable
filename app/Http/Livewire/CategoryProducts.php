<?php

namespace App\Http\Livewire;

use App\Http\Traits\IsTable;
use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;

class CategoryProducts extends Component
{
public $category;
    use  WithPagination, IsTable;
public function mount($category){
    $this->category = $category;
}

    public function render()
    {

        $results = Product::where('category',$this->category)->orderBy($this->columns[$this->orderBy], $this->orderDirection)
            ->paginate($this->paginationCount);
        return view('livewire.category-products', compact('results'));
    }
}
