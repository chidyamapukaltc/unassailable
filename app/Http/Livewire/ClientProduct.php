<?php

namespace App\Http\Livewire;

use App\Http\Traits\IsTable;
use Livewire\Component;
use Livewire\WithPagination;

class ClientProduct extends Component
{
    use  WithPagination, IsTable;
    public function render()
    {
        $products = \App\Models\Product::where(function ($q){
            $q->orwhere('name','LIKE','%' . $this->search . '%')
                ->orwhere('currency','LIKE','%' . $this->search .  '%')
                ->orwhere('category','LIKE','%' . $this->search .  '%');
        })
            ->orderBy($this->columns[$this->orderBy], $this->orderDirection)
            ->paginate($this->paginationCount);
        return view('livewire.client-product',compact('products'));
    }
}
