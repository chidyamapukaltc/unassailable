<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];
    public function productImage(){
        return $this->hasMany(ProductImage::class);
    }
    public function cartItem(){
        return $this->hasMany(CartItem::class);
    }
}
