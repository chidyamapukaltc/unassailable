<?php

use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\Client\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/Route::group(['prefix' => 'admin', 'middleware' => ['isAdmin','auth', 'PreventBackHistory']], function () {
    Route::get('/', [AdminController::class, 'index'])->name('admin.index');
    Route::get('admin_setting', [AdminController::class, 'settings'])->name('admin.setting');
    Route::get('admin_products', [AdminController::class, 'products'])->name('admin.products');

});

Route::get('/', [HomeController::class, 'index'])->name('client.index');
Route::get('/client-awareness-anxiety', [HomeController::class, 'clientAwarenessAnxiety'])->name('client.anxiety');
Route::get('/client-awareness-gbv', [HomeController::class, 'clientAwarenessGbv'])->name('client.gbv');
Route::get('/client-awareness-depression', [HomeController::class, 'clientAwarenessDepression'])->name('client.depression');
Route::get('/client-products', [HomeController::class, 'clientProducts'])->name('client.products');
Route::get('/category-products/{category}', [HomeController::class, 'categoryProduct'])->name('category.products');
Route::get('/client-news', [HomeController::class, 'clientNews'])->name('client.news');
Route::get('/client-faq', [HomeController::class, 'clientFaq'])->name('client.faq');
Route::get('/client-news-subscription', [HomeController::class, 'clientNewsSubscription'])->name('news.subscription');
Route::get('/client-product-details/{id}', [HomeController::class, 'clientFaq'])->name('client.product.detail');
Route::get('send-mail', [HomeController::class,'contactMail'])->name('send-mail');

Route::get('/client-awareness', [HomeController::class, 'clientAwareness'])->name('client.awareness');
Route::get('/client-checkout', [HomeController::class, 'clientCheckout'])->name('client.checkout');
Route::get('/client-contact', [HomeController::class, 'clientContact'])->name('client.contact');
Route::get('/client-product-details/{id}', [HomeController::class, 'clientProductDetails'])->name('client.product.details');
Route::get('/client-news-details', [HomeController::class, 'add'])->name('client.news.details');

Route::group(['middleware' => ['auth','PreventBackHistory']], function() {
    Route::get('/client-cart', [HomeController::class, 'clientCart'])->name('client.cart');

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
